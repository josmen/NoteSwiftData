//
//  Filters.swift
//  NoteSwiftData
//
//  Created by Jose Antonio Mendoza on 23/6/23.
//

import Foundation

enum NoteSortBy: Identifiable, CaseIterable {
    
    var id: Self { self }
    case createdAt
    case content
    
    var text: String {
        switch self {
            case .createdAt: "Created at"
            case .content: "Content"
        }
    }
    
}

enum OrderBy: Identifiable, CaseIterable {
    
    var id: Self { self }
    case ascending
    case descending
    
    var text: String {
        switch self {
            case .ascending: "Ascending"
            case .descending: "Descending"
        }
    }
}
