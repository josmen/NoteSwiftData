//
//  Tag.swift
//  NoteSwiftData
//
//  Created by Jose Antonio Mendoza on 23/6/23.
//

import Foundation
import SwiftData

@Model
final class Tag {
    
    var id: String?
    var name: String = ""
    
    @Relationship var notes: [Note]?
    @Attribute(.transient) var isChecked: Bool = false
    
    init(id: String, name: String, notes: [Note]) {
        self.id = id
        self.name = name
        self.notes = notes
    }
    
}
