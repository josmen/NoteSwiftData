//
//  NoteListView.swift
//  NoteSwiftData
//
//  Created by Jose Antonio Mendoza on 23/6/23.
//

import SwiftData
import SwiftUI

struct NoteListView: View {

    @Environment(\.modelContext) private var context
    @Query(sort: \.createdAt, order: .reverse) var allNotes: [Note]
    @Query(sort: \.name, order: .forward) var allTags: [Tag]
    @State private var noteText: String = ""

    var body: some View {
        List {
            Section {
                DisclosureGroup("Create a note") {
                    TextField("Enter text", text: $noteText, axis: .vertical)
                        .lineLimit(2...4)
                    
                    DisclosureGroup("Tag With") {
                        if allTags.isEmpty {
                            Text("You don't have any tags yet. Please create one from Tags tab")
                                .foregroundStyle(.gray)
                        }
                        
                        ForEach(allTags) { tag in
                            HStack {
                                Text(tag.name)
                                if tag.isChecked {
                                    Spacer()
                                    Image(systemName: "checkmark.circle")
                                        .symbolRenderingMode(.multicolor)
                                }
                            }
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .contentShape(Rectangle())
                            .onTapGesture {
                                withAnimation {
                                    tag.isChecked.toggle()
                                }
                            }
                        }
                    }
                    
                    Button("Save", action: createNote)
                }
            }
            
            Section {
                if allNotes.isEmpty {
                    ContentUnavailableView("You don't have any note yet", systemImage: "note")
                } else {
                    ForEach(allNotes) { note in
                        VStack(alignment: .leading, content: {
                            Text(note.content)
                            if let tags = note.tags, tags.count > 0 {
                                Text("Tags: " + tags.map { $0.name }.joined(separator: ", "))
                                    .font(.caption)
                            }
                            Text(note.createdAt, style: .time)
                                .font(.caption)
                        })
                    }
                    .onDelete(perform: { indexSet in
                        indexSet.forEach { index in
                            context.delete(allNotes[index])
                        }
                    })
                }
            }
        }
    }
    
    private func createNote() {
        var tags: [Tag] = []
        allTags.forEach { tag in
            if tag.isChecked {
                tags.append(tag)
                tag.isChecked = false
            }
        }
        let note = Note(id: UUID().uuidString, content: noteText, createdAt: .now, tags: tags)
        withAnimation {
            context.insert(note)
        }
        noteText = ""
    }
}

#Preview {
    NoteListView()
}
