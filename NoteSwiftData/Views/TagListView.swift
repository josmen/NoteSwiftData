//
//  TagListView.swift
//  NoteSwiftData
//
//  Created by Jose Antonio Mendoza on 23/6/23.
//

import SwiftData
import SwiftUI

struct TagListView: View {
    
    @Environment(\.modelContext) private var context
    @Query(sort: \.name, order: .reverse) var allTags: [Tag]
    @State private var tagText: String = ""

    var body: some View {
        List {
            Section {
                DisclosureGroup("Create a tag") {
                    TextField("Enter text", text: $tagText, axis: .vertical)
                        .lineLimit(2...4)
                    Button("Save", action: createTag)
                }
            }
            
            Section {
                if allTags.isEmpty {
                    ContentUnavailableView("You don't have any tag yet", systemImage: "tag")
                } else {
                    ForEach(allTags) { tag in
                        if let notes = tag.notes, notes.count > 0 {
                            DisclosureGroup("\(tag.name) (\(notes.count))") {
                                ForEach(notes) { note in
                                    Text(note.content)
                                }
                                .onDelete { indexSet in
                                    indexSet.forEach { index in
                                        context.delete(notes[index])
                                    }
                                }
                            }
                        } else {
                            Text(tag.name)
                        }
                    }
                    .onDelete(perform: { indexSet in
                        indexSet.forEach { index in
                            context.delete(allTags[index])
                        }
                    })
                }
            }
        }
    }
    
    private func createTag() {
        let tag = Tag(id: UUID().uuidString, name: tagText, notes: [])
        withAnimation {
            context.insert(tag)
        }
        tagText = ""
    }
}

#Preview {
    TagListView()
}
